import { Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { PostListComponent } from './post/post-list/post-list.component';
import { PostDetailsComponent } from './post/post-details/post-details.component';
import { PostCommentsComponent } from './post/post-comments/post-comments.component';
import { PostAddComponent } from './post/post-add/post-add.component';

export const AppRoutes: Routes = [

  {
    path: 'post/list',
    component: PostListComponent,
  },
  {
    path: 'post/details',
    component: PostDetailsComponent,
  },
  {
    path: 'post/comments',
    component: PostCommentsComponent,
  },
  {
    path: 'post/add',
    component: PostAddComponent,
  },
  {
    path: '',
    redirectTo: 'post/list',
    pathMatch: 'full'
  },

];