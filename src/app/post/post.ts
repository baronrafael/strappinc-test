//Modelo para manejar los datos de los posts a la hora de agregar o editar
export class Post {
    userId: number;
    id: number;
    title: string;
    body: string;
}