import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';

import { PostService } from '../post.service';

@Component({
  selector: 'app-post-comments',
  templateUrl: './post-comments.component.html',
  styleUrls: ['./post-comments.component.css']
})
export class PostCommentsComponent implements OnInit {

  //Id del que obtenemos los comentarios
  id: string
  //Objeto para manejar los comentarios a mostrar
  comments: any;

  //Se inyectan los servicios necesarios
  constructor(
    private router: Router,
    public snackBar: MatSnackBar,
    private postService: PostService
  ) { }

  ngOnInit() {
    //Verificamos si hay un id para obtener comentarios, sino volvemos al listado
    if(!this.checkId()){
      this.goToList();
      return;
    }
    //Si hay, obtenemos los comentarios
    this.id = localStorage.getItem("id");
    this.getComments();
    localStorage.removeItem("id");
  }

  //SnackBar para controlar mensajes mostrados al usuario
  openSnackBar(message: string) {
    this.snackBar.open(message, "Close", {
      duration: 3000,
    });
  }

  //Metodos de rutas
  goToList(){
    this.router.navigate(['']);
  }
  goToDetails(id: string){
    localStorage.setItem("id", id);
    this.router.navigate(['post/details']);
  }

  //Verificamos si hay id para obtener comentarios
  checkId(){
    if(!localStorage.getItem("id")){
      return false;
    }
    else {
      return true;
    }
  }

  //Obtenemos los comentarios y los guardamos en el objeto
  getComments(){
    this.postService.getPostComments(localStorage.getItem("id"))
    .subscribe(
      response =>{
        console.log(response);
        this.comments = response;
      },
      err =>{
        console.log(err);
        this.openSnackBar("Algo salió mal al obtener los comentarios...");
      }
    );
  }

}
