import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { MatDialog, MatSnackBar } from '@angular/material';

import { PostService } from '../post.service'
import { PostDeleteComponent } from '../post-delete/post-delete.component';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {

  //String para manejar el id del post del que obtendremos los detalles
  id: string;
  //Objeto en el que guardaremos el post obtenido
  post: any;

  //Se inyectan los servicios necesarios
  constructor(
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private postService: PostService
  ) 
  { }

  ngOnInit() {
    //Verificamos si hay un id para obtener detalles, sino volvemos al listado
    if(!this.checkDetails()){
      this.goToList();
      return;
    }
    //Si hay, obtenemos los detalles
    this.id = localStorage.getItem("id");
    this.getPostDetails();
    localStorage.removeItem("id");
  }

  //SnackBar para controlar mensajes mostrados al usuario
  openSnackBar(message: string) {
    this.snackBar.open(message, "Close", {
      duration: 3000,
    });
  }

  //Metodos de rutas
  goToList(){
    this.router.navigate(['']);
  }
  goToComments(){
    localStorage.setItem("id", this.id);
    this.router.navigate(['post/comments']);
  }
  goToEdit(post: any){
    localStorage.setItem("post", JSON.stringify(post));
    this.router.navigate(['post/add']);
  }

  //Metodo para abrir el MatDialog de Eliminar
  openDeleteDialog(){
    let dialog = this.dialog.open(PostDeleteComponent, {
      data: { id: this.id },
    });
  }

  //Verificamos si hay id para obtener detalles
  checkDetails(){
    if(!localStorage.getItem("id")){
      return false;
    }
    else {
      return true;
    }
  }

  //Obtenemos los detalles
  getPostDetails(){
    this.postService.getPostDetails(localStorage.getItem("id"))
    .subscribe(
      response =>{
        //console.log(response);
        this.post = response;
      },
      err =>{
        //console.log(err);
        this.openSnackBar("Algo salió mal al obtener los detalles...");
      }
    );
  }

}
