//Servicio para manejar las peticiones HTTP de los posts
import { Injectable } from '@angular/core';
import  { HttpClient, HttpHeaders } from '@angular/common/http';

const URL = "http://jsonplaceholder.typicode.com/";

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private httpclient: HttpClient
  ) 
  {  }

  getPosts(){
    return this.httpclient.get(URL+"posts");
  }

  getPostsByUserId(userId: string){
    return this.httpclient.get(URL+"posts?userId="+userId);
  }

  getPostDetails(id: string){
    return this.httpclient.get(URL+"posts/"+id);
  }

  getPostComments(id: string){
    return this.httpclient.get(URL+"posts/"+id+"/comments");
  }

  addPost(post: any){
    return this.httpclient.post(URL+"posts", post);
  }

  editPost(id: string, post: any){
    return this.httpclient.put(URL+"posts/"+id, post);
  }

  deletePost(id: string){
    return this.httpclient.delete(URL+"posts/"+id);
  }
}
