import { Component, Inject, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { PostService } from '../post.service'

@Component({
  selector: 'app-post-delete',
  templateUrl: './post-delete.component.html',
  styleUrls: ['./post-delete.component.css']
})
export class PostDeleteComponent implements OnInit {

  //Booleano para controlar las peticiones http
  bDelete: boolean = false;

  //Se inyectan los servicios necesarios
  constructor(
    private router: Router,
    public snackBar: MatSnackBar,
    private postService: PostService,
    @Inject(MAT_DIALOG_DATA) public post: any,
    public dialogRef: MatDialogRef<PostDeleteComponent>
    
  ) { }

  ngOnInit() { }

  //SnackBar para controlar mensajes mostrados al usuario
  openSnackBar(message: string) {
    this.snackBar.open(message, "Close", {
      duration: 3000,
    });
  }

  //Metodos de rutas
  goToList(){
    this.router.navigate(['']);
  }

  //Metodo para cerrar el MatDialog
  closeDialog() {
    this.dialogRef.close();
  }

  //Eliminamos el post con el id que manejamos
  deletePost(){
    this.bDelete = true;
    this.postService.deletePost(this.post.id)
    .subscribe(
      response =>{
        this.bDelete = false;
        console.log(response);
        this.openSnackBar("Post eliminado!");
        this.closeDialog();
        this.goToList();
      },
      err =>{
        this.bDelete = false;
        console.log(err);
        this.openSnackBar("Algo salió mal...");
      }
    );
  }

}
