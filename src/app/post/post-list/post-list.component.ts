import { Component, OnInit } from '@angular/core';

import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';

import { MatSnackBar } from '@angular/material';

import { Router } from '@angular/router';

import { PostService } from '../post.service'

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  //Objeto para manejar los posts que mostraremos en el listado
  posts: any;
  //Columnas de la tabla
  displayedColumns = ['ID de Usuario', 'ID', 'Titulo', 'Cuerpo', 'Detalles'];
  //Forma para manejar el post que se buscara mediante el filtrado
  filterForm: FormGroup;
  //Booleano para controlar las peticiones http
  bSearch: boolean = false;

  //Se inyectan los servicios necesarios
  constructor(
    private router: Router,
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    private postService: PostService
  ) 
  { }

  ngOnInit() {
    //Obtenemos los posts
    this.getPosts();
    //Preparamos la forma para el filtrado
    this.filterForm = this.fb.group({
      userId: ['', Validators.required],
    });
  }

  //SnackBar para controlar mensajes mostrados al usuario
  openSnackBar(message: string) {
    this.snackBar.open(message, "Close", {
      duration: 3000,
    });
  }
  //Metodos de rutas
  goToDetails(id: string){
    localStorage.setItem("id", id);
    this.router.navigate(['post/details']);
  }

  //Obtenemos los posts
  getPosts(){
    this.postService.getPosts()
    .subscribe(
      response =>{
        //console.log(response);
        this.posts = response;
      },
      err =>{
        this.openSnackBar("Algo salió mal al obtener los posts...");
        //console.log(err);
      }
    );
  }

  //Obtenemlos los posts, unicamente del usuario que se desea
  filterByUserId(){
    this.bSearch = true;
    this.postService.getPostsByUserId(this.filterForm.controls.userId.value)
    .subscribe(
      response =>{
        this.bSearch = false;
        console.log(response);
        this.posts = response;
      },
      err =>{
        this.bSearch = false;
        this.openSnackBar("Algo salió mal al obtener los posts...");
        console.log(err);
      }
    );
  }

}
