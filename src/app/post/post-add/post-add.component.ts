import { Component, OnInit } from '@angular/core';

import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';

import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';

import { PostService } from '../post.service';

import { Post } from '../post';

@Component({
  selector: 'app-post-add',
  templateUrl: './post-add.component.html',
  styleUrls: ['./post-add.component.css']
})
export class PostAddComponent implements OnInit {

  //Objeto para trabajar con el post que se edita
  post: any;
  //Forma del Post
  postForm: FormGroup;
  //Booleano para controlar las peticiones http
  bAdd: boolean = false;

  //Se inyectan los servicios necesarios
  constructor(
    private router: Router,
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    private postService: PostService
  ) 
  { }

  ngOnInit() {
    //Verificamos si hay un post por editar
    if(this.checkPostToEdit()){
      this.post = JSON.parse(localStorage.getItem("post"));
      localStorage.removeItem("post");
      //console.log(this.post);
    }
    //Preparamos el postForm dependiendo de si se agrega o se edita
    this.prepareForm();   
  }

  //SnackBar para controlar mensajes mostrados al usuario
  openSnackBar(message: string) {
    this.snackBar.open(message, "Close", {
      duration: 3000,
    });
  }

  //Metodos de rutas
  goToList(){
    this.router.navigate(['']);
  }
  goToDetails(id: string){
    localStorage.setItem("id", id);
    this.router.navigate(['post/details']);
  }

  //Verificamos si vamos a editar o no
  checkPostToEdit(){
    if(localStorage.getItem("post")){
      return true;
    }
    else{
      return false;
    }
  }

  prepareForm(){
    //Inicializamos la forma, pero antes verificamos si:
    //Si es un post a editar, se inicializa con los valores del post
    //Sino se inicializa vacía
    if(this.post){
      this.postForm = this.fb.group({
        userId: [this.post.userId, Validators.required],
        title: [this.post.title, Validators.required],
        body: [this.post.body, Validators.required],
      });
    }
    else{
      this.postForm = this.fb.group({
        userId: ['', Validators.required],
        title: ['', Validators.required],
        body: ['', Validators.required],
      });
    }
  }

  addPost() {
    this.bAdd = true;
    let auxPost = new Post();
    //Agregamos un post
    if(!this.post){

      auxPost.userId = this.postForm.controls['userId'].value;
      auxPost.title = this.postForm.controls['title'].value;
      auxPost.body = this.postForm.controls['body'].value;

      this.postService.addPost(auxPost)
      .subscribe(
        response => {
          this.bAdd = false;
          console.log(response);
          this.openSnackBar("Post agregado exitosamente");
          this.goToList();
        },
        err => {
          this.bAdd = false;
          console.log(err);
          this.openSnackBar("Algo salió mal...");
        }
      );
    }

    //Editamos un post
    if(this.post){

      auxPost.userId = this.postForm.controls['userId'].value;
      auxPost.title = this.postForm.controls['title'].value;
      auxPost.body = this.postForm.controls['body'].value;

      this.postService.addPost(auxPost)
      .subscribe(
        response => {
          this.bAdd = false;
          console.log(response);
          this.openSnackBar("Post editado exitosamente");
          this.goToList();
        },
        err => {
          this.bAdd = false;
          console.log(err);
          this.openSnackBar("Algo salió mal...");
        }
      );
    }
  }

}
